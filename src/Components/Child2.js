import { useState } from "react";
const Child2 = ({ card }) => {
  //render value from counter to determine the number of cards that display
  const [color, setColor] = useState("white");
  const [view, setView] = useState("list");

  function changeColor() {
    setColor(color === "white" ? "black" : "white");
  }

  function layout() {
    setView(view === "list" ? "grid" : "list");
  }
  return (
    <div className="my-list">
      <h3>My List</h3>
      <div>
        <button type="button" onClick={layout}>
          Grid/List
        </button>
        <button type="button" onClick={changeColor}>
          Black/White
        </button>
        {/* card block start here */}
        <div className={view}>
          {card.map((data, index) => (
            <div key={index} className={color}>
              <h4>Card #{index + 1}</h4>
            </div>
          ))}
        </div>
        {/* card block ends here */}
      </div>
    </div>
  );
};

export default Child2;
