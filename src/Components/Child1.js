// import React from "react";
import { useState, useEffect } from "react";
import Child2 from "./Child2";

const Child1 = () => {
  let [counter, setCounter] = useState(0);
  const [click, setclick] = useState(0);

  useEffect(() => {
    if (click % 5 === 0 && click !== 0) {
      alert("You have update 5 times");
    }
  }, [click]);

  const updateCounter = (type) => {
    if (type === "increment") {
      setCounter((prevCounter) => prevCounter + 1);
    }

    if (type === "decrement") {
      if (counter > 0) {
        setCounter((prevCounter) => prevCounter - 1);
      }
    }

    if (type === "reset") {
      setCounter((counter = 0));
    }

    setclick((prevClick) => prevClick + 1);
  };

  const EvenOdd = counter % 2 === 0 ? "EVEN" : "ODD";
  return (
    <>
      <h3>Counter</h3>
      <div>Value is {EvenOdd}</div>
      <h2>{counter}</h2>
      <button onClick={() => updateCounter("increment")}>ADD</button>
      <button onClick={() => updateCounter("decrement")}>MIN</button>
      <button onClick={() => updateCounter("reset")}>RESET</button>
      <Child2 card={Array(counter).fill()} />
    </>
  );
};

export default Child1;
