import Child1 from "./Components/Child1";
import "./Assessment.css";

const Assessment = () => {
  return (
    <div className="App">
      <h1>Assessment Week 4</h1>
      <Child1 />
    </div>
  );
};
export default Assessment;
