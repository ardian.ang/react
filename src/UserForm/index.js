import { useState } from "react";
import { useContext } from "react";
import UserContext from "../UserContext";

export default function UserForm() {
  const [userInput, setUserInput] = useState("");
  const { changeUserName } = useContext(UserContext);

  const handleSubmit = (e) => {
    e.preventDefault();
    changeUserName(userInput);
    setUserInput("");
  };

  const handleChange = (e) => {
    setUserInput(e.target.value);
  };
  return (
    <form onSubmit={handleSubmit}>
      <input placeholder="change user" value={userInput} onChange={handleChange} />
      <button type="submit" value="submit">
        submit
      </button>
    </form>
  );
}
