import { useState } from "react";
import { Routes, Route, Link } from "react-router-dom";
import HelloWorld from "./HelloWorld/HelloWorld";
import Todo from "./Todo";
import Child1 from "./Child/Child1";
import Child2 from "./Child/Child2";
import Post from "./Axios/Post";
import PostDetail from "./Axios/PostDetail";
import styles from "./App.module.css";

import UserContext from "./UserContext";
import UserForm from "./UserForm";

function App() {
  const title = "Nama Saya ANG";
  const [count, setCount] = useState(0);

  const increment = () => {
    setCount((prevCount) => prevCount + 1);
  };

  const decrement = () => {
    if (count > 0) {
      setCount((prevCount) => prevCount - 1);
    }
  };

  const user = {
    name: "Ardian",
    changeUserName(name) {
      user.name = name;
    },
  };
  return (
    <>
      <UserContext.Provider value={"Ardian"}>
        <div className="App">
          <h1 className={styles.title}>Good Morning...!!!</h1>
          <nav style={{ display: "flex", gap: ".75rem" }}>
            <Link to="/">Home</Link>
            <Link to="/todo">Todo</Link>
            <Link to="/child">Child</Link>
            <Link to="/post">Post</Link>
          </nav>
          <Routes>
            <Route path="todo" element={<Todo />} />
            <Route path="child" element={<Child1 count={count} increment={increment} />} />
            <Route path="post" element={<Post />} />
            <Route path="detail/:id" element={<PostDetail />} />
            <Route path="/" element={<HelloWorld title={title} />} />
            <Route path="*" element={<Child2 count={count} decrement={decrement} />} />
          </Routes>
          {/* <Todo /> */}
          {/* <h1>Count App: {count}</h1> */}
          {/* <Child1 count={count} increment={increment} /> */}
          {/* <Child2 count={count} decrement={decrement} /> */}
        </div>
      </UserContext.Provider>
    </>
  );
}

export default App;
