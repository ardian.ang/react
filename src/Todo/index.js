import { useState, useEffect } from "react";

const Todo = () => {
  const [taskList, setTaskList] = useState([]);
  const [taskInput, setTaskInput] = useState("");

  useEffect(() => {
    console.log("useEffect tanpa parameter kedua");
  });

  useEffect(() => {
    loadTask();
    console.log("useEffect dengan parameter kedua berupa array kosong");
  }, []);

  useEffect(() => {
    console.log("useEffect dengan taskList sebagai depedency");
  }, [taskList]);

  useEffect(() => {
    return function cleanup() {
      console.log("function ini akan dipanggil ketika komponen unmount");
    };
  }, []);

  const loadTask = () => {
    setTaskList(["makan", "minum"]);
  };

  const deleteTodo = (index) => {
    setTaskList(taskList.filter((indexList) => indexList !== index));
  };

  const submitTodo = (evt) => {
    evt.preventDefault();
    setTaskList([...taskList, taskInput]);
    setTaskInput("");
  };

  return (
    <>
      <h2>Todo list</h2>
      <ol>
        {taskList.map((task, index) => (
          <li key={index}>
            {task}
            <button onClick={() => deleteTodo(index)}>delete</button>
          </li>
        ))}
      </ol>
      <form onSubmit={submitTodo}>
        <input value={taskInput} onChange={(evt) => setTaskInput(evt.target.value)}></input>
        <button type="submit" value="submit">
          add todo
        </button>
      </form>
    </>
  );
};
export default Todo;
