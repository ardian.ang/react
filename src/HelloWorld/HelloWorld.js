import { useContext } from "react";
import UserContext from "../UserContext";
import styles from "./HelloWorld.module.css";

const HelloWorld = ({ title }) => {
  const user = useContext(UserContext);
  return <h2 className={styles.title}>{title} {user}</h2>;
};

export default HelloWorld;
