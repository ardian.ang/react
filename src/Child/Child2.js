export default function Child2({ count, decrement }) {
  return (
    <>
      <h1>Count Child2: {count}</h1>
      <button onClick={decrement}>Min</button>
    </>
  );
}
