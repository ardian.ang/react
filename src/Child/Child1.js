export default function Child1({ count, increment }) {
  return (
    <>
      <h1>Count Child1: {count}</h1>
      <button onClick={increment}>Add</button>
    </>
  );
}
