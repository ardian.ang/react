import { Component } from "react";

class Todo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      // taskList: ['bangun tidur', 'mandi', 'masak', 'sarapan']
      taskList: [],
    };
    this.handleClick = this.handleClick.bind(this);
  }
  componentDidMount() {
    console.log("component berhasil di mount/render");
  }
  componentDidUpdate(prevProps, prevState) {
    console.log("component berhasil di-update");
  }
  componentWillUnmount() {
    console.log("component akan di-unmount");
  }

  handleClick() {
    console.log("button ter-click");
    this.setState({
      taskList: ["bangun tidur", "mandi", "masak", "sarapan"],
    });
  }

  handleClickArrow = () => {
    console.log("button ter-click(Arrow function)");
    this.setState({
      taskList: ["bangun tidur", "mandi", "masak", "sarapan"],
    });
  };

  //const arr2 = [0, 1, 2, 3]
  //const arr = arr2.map((value, index) => {
  //returnt value + 1
  //})
  //hasil arr = [1,2,3,4]

  render() {
    const { taskList } = this.state;
    return (
      <>
        <h2>Todo list</h2>
        <ol>
          {taskList.map((task, index) => (
            <li key={index}>{task}</li>
          ))}
        </ol>
        <button onClick={this.handleClick}>click</button>
        <button onClick={this.handleClickArrow}>click Arrow</button>
      </>
    );
  }
}

export default Todo;
