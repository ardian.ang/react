import axios from "axios";
import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";

export default function PostDetail() {
  const [detail, setDetail] = useState([]);
  const params = useParams();

  useEffect(() => {
    const getPostDetail = async () => {
      try {
        const result = await axios.get(`https://jsonplaceholder.typicode.com/posts/${params.id}`);
        // console.log(result);
        setDetail(result.data);
      } catch (error) {
        console.log(error);
      }
    };
    getPostDetail();
  }, [params.id]);
  return (
    <div>
      <h1>Halaman Post Detail</h1>
      <dt>
        {detail.id}: {detail.title}
      </dt>
      <dl>{detail.body}</dl>
    </div>
  );
}
