import axios from "axios";
import { useEffect, useState } from "react";
import { Link } from "react-router-dom";

export default function Post() {
  const [postList, setPostList] = useState([]);
  useEffect(() => {
    const getPostList = async () => {
      try {
        const result = await axios.get("https://jsonplaceholder.typicode.com/posts/");
        setPostList(result.data);
      } catch (error) {
        console.log(error);
      }
    };
    getPostList();
  }, []);

  // const handleSubmit = async (e) => {
  //   e.priventDefault();
  //   try {
  //     const result = await axios({
  //       method: "POST",
  //       url: "https://jsonplaceholder.typicode.com/posts",
  //       data: JSON.stringify({
  //         title: "foo",
  //         body: "bar",
  //         userId: 1,
  //       }),
  //       headers: {
  //         "Content-type": "application/json; charset=UTF-8",
  //       },
  //     });
  //    console.log(result);
  //   } catch (error) {
  //     console.log(error);
  //   }
  // };
  return (
    <>
      {/* <form onSubmit={handleSubmit}>
        <label>
          type:
          <input />
        </label> */}
        {/* <label>body:
        <input type="text" id="body" />
        </label> */}
        {/* <button type="submit" value="Submit">
          Add
        </button>
      </form> */}
      <h1>Halaman Post</h1>
      <dl>
        {postList.map((post) => (
          <div key={post.id}>
            <dt>
              <Link to={`/detail/${post.id}`}>{post.title}</Link>
            </dt>
            <dd>{post.body}</dd>
          </div>
        ))}
      </dl>
    </>
  );
}
