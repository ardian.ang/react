import { createContext } from "react";
const user = {
    name: 'Ardian',
    changeUserName(name){
        user.name = name
    }
}
const UserContext = createContext(user);
export default UserContext;
